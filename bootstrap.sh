#!/bin/sh

cd /var/www/html/app
cp /var/www/html/app/.env.example /var/www/html/app/.env
php /var/www/html/app/artisan key:generate
php /var/www/html/app/artisan migrate
php /var/www/html/app/artisan serve --host 0.0.0.0